class Post < ApplicationRecord

  validates :name,  :presence => true
  validates :title, :presence => true

  acts_as_votable

  has_many :comments, as: :commentable, :dependent => :destroy
  has_many :taggings
  has_many :tags, through: :taggings

  accepts_nested_attributes_for :tags, :allow_destroy => :true,
                                :reject_if => proc { |attrs| attrs.all? { |k, v| v.blank? } }
  def self.tagged_with(name)
    Tag.find_by_name!(name).posts
  end
  def all_tags=(names)
    self.tags = names.split(",").map do |name|
      Tag.where(name: name.strip).first_or_create!
    end
  end

  def all_tags
    self.tags.map(&:name).join(", ")
  end
end
