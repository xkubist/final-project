class User < ApplicationRecord
  has_secure_password

  acts_as_voter
  validates :first_name,  :presence => true
  validates :last_name,  :presence => true
  validates :email,  :presence => true,
                    :uniqueness =>true,
                    format: {
                        with: /\b[A-Z0-9._%a-z\-]+@(?:[A-Z0-9a-z\-]+\.)+[A-Za-z]{2,4}\z/
                    }
  has_many :posts
  def to_s
    "#{first_name} #{last_name}"
  end
end
