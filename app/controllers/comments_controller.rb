class CommentsController < ApplicationController
  #before_action :current_user, :except => [:index, :show]
  before_action :find_commentable

  def new
    @comment = Comment.new
  end

  def create
    @comment = @commentable.comments.new comment_params

    if @comment.save
      redirect_to :back, notice: 'Your comment was successfully posted!'
    else
      redirect_to :back, notice: "Your comment wasn't posted!"
    end
  end

  def destroy
    @comment = @commentable.comments.find(params.require(:id))
    @comment.destroy
    redirect_to :back, :notice => 'Comment was deleted'
  end

  def show
    @comment = @commentable.comments.find(params.require(:id))
  end

  def upvote
    @comment = @commentable.comments.find(params.require(:id))
    @comment.liked_by current_user
    redirect_to :back
  end

  def downvote
    @comment = @commentable.comments.find(params.require(:id))
    @comment.downvote_from current_user
    redirect_to :back
  end

  private

  def comment_params
    params.require(:comment).permit(:body, :commenter)
  end

  def find_commentable
    @commentable = Comment.find_by_id(params[:comment_id]) if params[:comment_id]
    @commentable = Post.find_by_id(params[:post_id]) if params[:post_id]
  end

end
