class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  private

  def current_user
    User.where(id: session[:user_id]).first
  end

  def owned(post)
    if current_user
      if !admin
      current_user.id == post.user_id
      else
        false
      end
    end
  end

  def comment_owned(comment)
    if current_user
      if !admin
        current_user.email == comment.commenter
      else
        false
      end
    end
  end

  def admin()
    current_user.admin==1
  end
  helper_method :current_user, :owned, :admin, :comment_owned
end
