Rails.application.routes.draw do
  post '/rate' => 'rater#create', :as => 'rate'
  resources :sessions, only: [:new, :create, :destroy]
  
  get 'singup', to: 'users#new', as: 'singup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  get 'profile', to: 'users#edit', as: 'profile'
  get 'users', to: 'users#index', as: 'users'
  get 'home', to: 'posts#index', as: 'home'
  get 'tags/:tag', to: 'posts#index', as: "tag"

  resources :users do
    resources :posts
  end
  resources :posts do
    member do
      put "like", to: "posts#upvote"
      put "dislike", to: "posts#downvote"
    end
    resources :comments
  end
  resources :comments do
    member do
      put "like", to: "comments#upvote"
      put "dislike", to: "comments#downvote"
    end
    resources :comments
  end

  get 'home/index'
  root "posts#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
